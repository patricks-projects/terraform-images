.terraform-test-base:
  image: "$TERRAFORM_IMAGE_NAME"
  before_script:
    - gitlab-terraform version
    - jq --version
  cache:
    key: "$TERRAFORM_VERSION-$CI_COMMIT_REF_SLUG"
    paths:
      - tests/.terraform/

.terraform-test:
  extends:
    - .terraform-test-base
  before_script:
    - !reference [.terraform-test-base, before_script]
    - cd tests

.test-tf-root:
  extends:
    - .terraform-test-base
  variables:
    TF_ROOT: tests

terraform-test-init:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

terraform-test-init-with-args:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init -get=true -no-color

terraform-test-init-with-flags:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true -no-color"
    - gitlab-terraform init

terraform-test-init-with-flags-and-args:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true"
    - gitlab-terraform init -no-color

terraform-test-init-tf-root:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

terraform-test-init-tf-root-with-cd:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - cd tests
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

terraform-test-init-tf-root-with-args:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init -get=true -no-color

terraform-test-init-tf-root-with-flags:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true -no-color"
    - gitlab-terraform init

terraform-test-init-tf-root-with-flags-and-args:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true"
    - gitlab-terraform init -no-color

terraform-test-init-without-reconfigure:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - gitlab-terraform init
    - |
      cat <<EOF > $TF_ROOT/backend_override.tf
      terraform {
        backend "local" {}
      }
      EOF
    - export TF_INIT_NO_RECONFIGURE=true
    - FAILED=false
    - gitlab-terraform init -no-color >/tmp/output.txt 2>&1 || FAILED=true
    - cat /tmp/output.txt
    - test $FAILED = true
    - 'cat /tmp/output.txt | grep "Error: Backend configuration changed"'

terraform-test-init-with-reconfigure:
  extends:
    - .test-tf-root
    - .terraform-versions
  stage: test-init
  script:
    - gitlab-terraform init
    - |
      cat <<EOF > $TF_ROOT/backend_override.tf
      terraform {
        backend "local" {}
      }
      EOF
    - gitlab-terraform init

terraform-test-init-with-prepared-registry-token:
  extends:
    - .terraform-test
  stage: test-init
  variables:
    TERRAFORM_VERSION: $STABLE_VERSION
  script:
    - apk add --update $PKG
    - |
      cat <<'EOF' > test.sh
      set -x
      export TF_TOKEN_gitlab_com=mysecrettoken
      . $(which gitlab-terraform)
      terraform_authenticate_private_registry
      test "$TF_TOKEN_gitlab_com" = "mysecrettoken"
      EOF
    - $SHELL test.sh
  parallel:
    matrix:
      - SHELL: "bash"
        PKG: "bash"
      - SHELL: "zsh"
        PKG: "zsh"
      - SHELL: "ksh"
        PKG: "loksh"

terraform-test-init-without-prepared-registry-token:
  extends:
    - .terraform-test
  stage: test-init
  variables:
    TERRAFORM_VERSION: $STABLE_VERSION
  script:
    - apk add --update $PKG
    - |
      cat <<'EOF' > test.sh
      set -x
      . $(which gitlab-terraform)
      terraform_authenticate_private_registry
      test -n "$TF_TOKEN_gitlab_com"
      EOF
    - $SHELL test.sh
  parallel:
    matrix:
      - SHELL: "bash"
        PKG: "bash"
      - SHELL: "zsh"
        PKG: "zsh"
      - SHELL: "ksh"
        PKG: "loksh"

terraform-test-fmt:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-fmt
  script:
    - gitlab-terraform fmt

terraform-test-validate:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-validate
  script:
    - gitlab-terraform validate

terraform-test-plan:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-plan
  variables:
    TF_PLAN_CACHE: $TERRAFORM_VERSION-plan.cache
  script:
    - gitlab-terraform plan
    - if [[ ! -f "$TERRAFORM_VERSION-plan.cache" ]]; then echo "expected to find a plan.cache file"; exit 1; fi
    - gitlab-terraform plan-json
    - if [[ ! -f "plan.json" ]]; then echo "expected to find a plan.json file"; exit 1; fi
  artifacts:
    paths:
      - "tests/*-plan.cache"

terraform-test-apply:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-apply
  variables:
    TF_PLAN_CACHE: $TERRAFORM_VERSION-plan.cache
  script:
    - gitlab-terraform apply

terraform-test-destroy:
  extends:
    - .terraform-test
    - .terraform-versions
  stage: test-destroy
  script:
    - gitlab-terraform destroy

terraform-test-source-script:
  extends:
    - .terraform-test
  stage: test-misc
  needs: [build terraform]
  variables:
    TERRAFORM_VERSION: $STABLE_VERSION
  before_script:
    - !reference [.terraform-test-base, before_script]
    - apk add --update $PKG
  script:
    - |
      cat <<'EOF' > test.sh
      set -x
      test -z "$TF_GITLAB_SOURCED"
      . $(which gitlab-terraform)
      test $TF_GITLAB_SOURCED
      EOF
    - |
      mkdir /usr/local/sbin
      cat <<'EOF' > /usr/local/sbin/terraform
      #/!usr/bin/env sh -e
      echo "Called Terraform, but shouldn't have!!"
      false
      EOF
      chmod +x /usr/local/sbin/terraform
    - $SHELL test.sh
  parallel:
    matrix:
      - SHELL: "bash"
        PKG: "bash"
      - SHELL: "zsh"
        PKG: "zsh"
      - SHELL: "ksh"
        PKG: "loksh"

terraform-test-without-implicit-init:
  extends:
    - .terraform-test
  stage: test-misc
  needs: [build terraform]
  cache:
  variables:
    TERRAFORM_VERSION: $STABLE_VERSION
    STATE_NAME: $CI_JOB_NAME
  script:
    - export TF_IMPLICIT_INIT=false
    - FAILED=false
    - gitlab-terraform $CMD -no-color >/tmp/output.txt 2>&1 || FAILED=true
    - cat /tmp/output.txt
    - test $FAILED = true
    - 'cat /tmp/output.txt | grep "$ERROR"'
  parallel:
    matrix:
      - CMD: apply
        ERROR: 'Error: Failed to load "plan.cache" as a plan'
      - CMD: destroy
        ERROR: 'Error: Backend initialization required, please run "terraform init"'
      - CMD: plan
        ERROR: 'Error: Backend initialization required, please run "terraform init"'
      - CMD: validate
        ERROR: 'Run "terraform init" to install all modules'

terraform-test-no-wrapper:
  extends:
    - .terraform-test
  stage: test-misc
  needs: [build terraform]
  cache:
  variables:
    TERRAFORM_VERSION: $STABLE_VERSION
    STATE_NAME: $CI_JOB_NAME
  script:
    # NOTE: running `gitlab-terraform apply` wouldn't fail
    #       because of the implicit `terraform init`.
    - FAILED=false
    - gitlab-terraform -- apply -no-color >/tmp/output.txt 2>&1 || FAILED=true
    - cat /tmp/output.txt
    - test $FAILED = true
    - 'cat /tmp/output.txt | grep "Error: Backend initialization required, please run \"terraform init\""'

terraform-integration-test-template:
  stage: test-integration
  variables:
    IMAGE: $TERRAFORM_IMAGE_NAME
    TERRAFORM_VERSION: $STABLE_VERSION
    TF_STATE_NAME: ci-terraform-integration-test-template-$CI_PIPELINE_IID-$CI_NODE_INDEX
    TF_ROOT: tests
  trigger:
    include: .gitlab/integration-test/Test-$TEMPLATE
    strategy: depend
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/terraform-images"'
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'
  parallel:
    matrix:
      - TEMPLATE: [Terraform.gitlab-ci.yml, Terraform.latest.gitlab-ci.yml]
